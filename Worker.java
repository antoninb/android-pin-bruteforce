import java.security.*;


public class Worker extends Thread {
	
	public static long salt = 2486025487081881776L;
	//public static long salt = 8942259654351833324L;
	public static String saltH = Long.toHexString(salt);
	
	public static String pass = "610E258508BF6C2632F1BE1B67F1EF22CAA4FB963AE4A0172D3220D9DB8B577F74839F81";
	//public static String pass = "0B5BE637F0D5D3AB9A617304550FB18792B60CD50966CEBEFD36E03F103F2CE8CF445E33";
	public static byte[] passB = pass.getBytes();
	
	public String digit;
	
	public Worker(int d){
		digit = d + "";
	}
	
	public static boolean passwordToHash(String password) {
        if (password == null) {
            return false;
        }
        String algo = null;
        byte[] hashed = null;
        try {
            byte[] saltedPassword = (password + saltH).getBytes();
            byte[] sha1 = MessageDigest.getInstance(algo = "SHA-1").digest(saltedPassword);
            byte[] md5 = MessageDigest.getInstance(algo = "MD5").digest(saltedPassword);
            hashed = (toHex(sha1) + toHex(md5)).getBytes();
        } catch (NoSuchAlgorithmException e) {
            //Log.w(TAG, "Failed to encode string because of missing algorithm: " + algo);
        }
        for(int j = 0; j < passB.length; j++){
        	if(passB[j] != hashed[j]) return false;
        }
        return true;
}

	private static String toHex(byte[] ary) {
        final String hex = "0123456789ABCDEF";
        String ret = "";
        for (int i = 0; i < ary.length; i++) {
            ret += hex.charAt((ary[i] >> 4) & 0xf);
            ret += hex.charAt(ary[i] & 0xf);
        }
        return ret;
    }
	
	public void run() {
		long prev = 10;
        for(long i = 0; i < Long.MAX_VALUE; i++){
        	if(passwordToHash(digit + i)){
        		System.out.println("FOUND:  " + (digit + i));
        	}
        	if(i == prev){
        		System.out.println("Worker " + digit + " is at step " + i);
        		prev = 10 * prev;
        	}
        }
    }

}
